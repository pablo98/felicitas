#!/usr/bin/env python

import pickle
import sys

import numpy as np
import pandas as pd

import felicitas.predict as predict


def main():
    features = collect_features()
    price = predict.predict_from_features(features)
    print("I believe that property is worth {} EUR".format(price))


def collect_features():
    return {f: input(f + " : ") for f in predict.input_features}

if __name__ == "__main__":
    main()
