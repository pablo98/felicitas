#!/usr/bin/env python

# import requests_cache
import felicitas.scrap as scrap

# cache to avoid being caught
# requests_cache.install_cache('funda_cache')

# list up searches
search_paths = [
    '/koop/amsterdam/3-dagen/'
]

# list up individual properties
properties_paths = [
#    '/koop/amsterdam/appartement-49003065-gibraltarstraat-66-hs/']
#    '/koop/verkocht/amsterdam/appartement-49968858-bestevaerstraat-5-hs/'
]

# scrap it out
scrap.scrap_searches(search_paths)

for path in properties_paths:
    scrap.scrap_property(path)
