#!/usr/bin/env python

import os
from osgeo import ogr, osr

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')

# define input
shape_file = os.path.join(PROJECT_DIR, 'data/woningwaarde/WONINGWAARDE_2015_INFLATIE_region.shp')
o_lat = 52.3605883
o_lon = 4.8593157

# geospatial bureocracy
driver = ogr.GetDriverByName('ESRI Shapefile')
shape = driver.Open(shape_file)
layer = shape.GetLayer()
geo_ref = layer.GetSpatialRef()
point_ref = ogr.osr.SpatialReference()
point_ref.ImportFromEPSG(4326)
ctran = ogr.osr.CoordinateTransformation(point_ref, geo_ref)

# critical part: transform longitude/latitude to the shapefile's projection
[t_lon, t_lat, z] = ctran.TransformPoint(o_lon, o_lat)

# create the needle
point = ogr.Geometry(ogr.wkbPoint)
point.SetPoint_2D(0, t_lon, t_lat)
# layer.SetSpatialFilter(point)

# look it up
for feature in layer:
    polygon = feature.GetGeometryRef()
    dist = polygon.Distance(point)
    print('some layer is this far from the point', dist)
    if polygon.Contains(point):
        print('Found it', feature.ExportToJson())
