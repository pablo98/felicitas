#!/bin/bash

scp data/*.pickle filomena@amsterprice.com:/home/filomena/felicitas/data
scp predictors/predictor.pickle filomena@amsterprice.com:/home/filomena/felicitas/predictors
