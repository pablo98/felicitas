import os
import pickle


class PostcodeMapper(object):

    def __init__(self, pickle_filename):
        self.pickle_filename = pickle_filename
        self.load()

    def get(self, postcode):
        # handle miss
        if postcode not in self.data:
            data = self._get(postcode)

            # Don't save thrash
            if data is None:
                return None

            self.data[postcode] = data
            self.save()

        return self.data[postcode]

    def save(self):
        with open(self.pickle_filename, 'wb') as fh:
            pickle.dump(self.data, fh, pickle.HIGHEST_PROTOCOL)

    def load(self):
        if not os.path.exists(self.pickle_filename):
            self.data = {}
            return

        with open(self.pickle_filename, 'rb') as fh:
            self.data = pickle.load(fh)

    def _get(self, postcode):
        return None
