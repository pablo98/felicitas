import requests
import time


from felicitas.postcode_mapper import PostcodeMapper


class PostcodeGeoMapper(PostcodeMapper):
    api_url = 'http://maps.google.com/maps/api/geocode/json'

    def _get(self, postcode):
        # @hack for throttling
        time.sleep(0.1)

        try:
            params = {'address': postcode}
            response = requests.get(self.api_url, params=params)
            coords = response.json()['results'][0]['geometry']['location']
            return float(coords['lat']), float(coords['lng'])

        except Exception as e:
            print(str(e))
            return None
