import os
import json
from osgeo import ogr, osr

from felicitas.postcode_mapper import PostcodeMapper

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')


class PostcodePriceMapper(PostcodeMapper):
    shape_file = os.path.join(PROJECT_DIR, 'data/woningwaarde/WONINGWAARDE_2015_INFLATIE_region.shp')

    def __init__(self, pickle_filename, post2geo_mapper):
        super().__init__(pickle_filename)

        self.post2geo_mapper = post2geo_mapper

        # geospatial bureocracy
        self.driver = ogr.GetDriverByName('ESRI Shapefile')
        self.shape = self.driver.Open(self.shape_file)
        self.layer = self.shape.GetLayer()
        self.geo_ref = self.layer.GetSpatialRef()
        self.point_ref = ogr.osr.SpatialReference()
        self.point_ref.ImportFromEPSG(4326)
        self.ctran = ogr.osr.CoordinateTransformation(self.point_ref, self.geo_ref)

    def _get(self, postcode):
        lat, lng = next(self.post2geo_mapper(postcode))

        if lat is None or lng is None:
            return None

        feature = self._get_closest_feature(lng, lat)
        if feature is None:
            return None

        return self._get_price_from_feature(feature)

    @staticmethod
    def _get_price_from_feature(feature):
        feature = json.loads(feature.ExportToJson())
        return int((feature['properties']['Lower'] + feature['properties']['Upper']) / 2)

    def _get_closest_feature(self, lng, lat):
        #  transform longitude/latitude to that of the shapefile's
        [t_lng, t_lat, z] = self.ctran.TransformPoint(lng, lat)
        # create the needle
        point = ogr.Geometry(ogr.wkbPoint)
        point.SetPoint_2D(0, t_lng, t_lat)

        # look for closest one
        min_dist = 1000000
        closest_feature = None
        for feature in self.layer:
            polygon = feature.GetGeometryRef()
            dist = polygon.Distance(point)
            if dist < min_dist and dist < 500:
                min_dist = dist
                closest_feature = feature

        return closest_feature


    def _get_container_feature(self, lng, lat):
        #  transform longitude/latitude to that of the shapefile's
        [t_lng, t_lat, z] = self.ctran.TransformPoint(lng, lat)
        # create the needle
        point = ogr.Geometry(ogr.wkbPoint)
        point.SetPoint_2D(0, t_lng, t_lat)
        self.layer.SetSpatialFilter(point)

        # look it up
        for feature in self.layer:
            polygon = feature.GetGeometryRef()
            if polygon.Contains(point):
                return feature

        return None
