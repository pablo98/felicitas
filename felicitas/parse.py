import re
import pickle

import pandas as pd
from bs4 import BeautifulSoup

from . import fetch


def get_property_soup(filename):
    with open(filename, 'r') as f:
        soup = BeautifulSoup(f.read(), "lxml")

        # @hack we pollute the beautifulsoup with an extra key
        soup['property_name'] = filename.split('/')[-1]

    return soup

# list of all feature names we parse from the html files
feature_names = [
    'name',
    'address',
    'price',
    'postcode',
    'areacode',
    'energielabel',
    'floor_number',
    'lift',
    'indoor_area',
    'garden_area',
    'annual_lease_amount',
    'lat',
    'lng',
    'is_ground_floor',
    'is_binnen_de_ring',
    'is_not_binnen_de_ring',
    'has_erfpacht',
    'lift',
    'area_price_per_mt2',
    'price_per_mt2',
    'price_per_mt2_vs_area',
    'postcode_asking_price_per_mt2']


def get_postcode_and_meter_price(soup):
    postcode = parse_postcode(soup)
    price = parse_price(soup)
    meters = parse_indoor_area(soup)

    if not postcode or not price or not meters:
        return None

    return {
        'postcode': postcode,
        'areacode': postcode[:4],
        'price_per_mt2': price / meters
    }


def get_features_from_soup(soup):
    fs = {}

    # html parsing
    fs['name'] = soup['property_name']
    price = parse_price(soup)
    fs['price'] = price
    postcode = parse_postcode(soup)
    fs['areacode'] = postcode[:4]
    fs['energielabel'] = parse_energie_label(soup)
    fs['postcode'] = postcode
    fs['address'] = parse_address(soup)
    fs['indoor_area'] = parse_indoor_area(soup)
    fs['floor_number'] = parse_floor_number(soup)
    fs['garden_area'] = parse_backgarden_area(soup)
    _, fs['annual_lease_amount'] = parse_annual_lease(soup)
    fs['lift'] = parse_lift(soup)

    # post-processing
    fs['lat'], fs['lng'] = next(fetch.get_coords_from_postcode(postcode))
    fs['area_price_per_mt2'] = next(fetch.get_price_per_mt2_from_postcode(postcode))
    fs['postcode_asking_price_per_mt2'] = next(fetch.get_asking_price_per_mt2_from_postcode(postcode))
    fs['is_ground_floor'] = 1 if fs['floor_number'] == 0 else 0
    fs['has_erfpacht'] = 1 if fs['annual_lease_amount'] else 0
    fs['is_binnen_de_ring'] = 1 if fetch.is_binnen_de_ring(fs['areacode']) else 0
    fs['is_not_binnen_de_ring'] = 0 if fs['is_binnen_de_ring'] else 1

    if price and fs['indoor_area']:
        fs['price_per_mt2'] = price / fs['indoor_area']

    return fs

def normalise_postcode(postcode):
    return postcode[0:4] + postcode[5:7]


def get_features_from_file(filename):
    return get_features_from_soup(get_property_soup(filename))


def is_property_representative(soup):
    if not (
        soup['property_name'].startswith('huis') or
        soup['property_name'].startswith('appartement')
    ):
        return False

    offered_since = _parse_label(soup, 'Aangeboden sinds')
    if offered_since and is_offer_withering(offered_since):
        print('Skipping due to withering.')
        return False

    if is_woonboot(soup):
        print('Skipping due to boat house')
        return False

    return True


def is_offer_withering(offered_since):
    offered_since = offered_since.lower()
    if (
        offered_since.startswith('6+ maanden') or
        offered_since.startswith('5 maanden') or
        offered_since.startswith('4 maanden') or
        offered_since.startswith('3 maanden')
    ):
        return True

    return False


def get_dataframe_from_html_features(features):
    feature_dict = {k: [r[k] for r in features] for k in features[0].keys()}
    df = pd.DataFrame.from_dict(feature_dict)

    return df


def parse_properties_meter_price_per_postcode(files):
    for f in files:
        soup = get_property_soup(f)
        if not is_property_representative(soup):
            continue

        features = get_postcode_and_meter_price(soup)
        if features is None:
            continue

        print('Parsed postcode and meter price ok in' + f)
        yield features


def parse_properties_files(files):
    ''' Parse individual features '''
    missing_features = {}
    q_parseable = q_parsed = 0
    for f in files:
        print('\nwill parse {}'.format(f))
        soup = get_property_soup(f)
        if not is_property_representative(soup):
            continue

        q_parseable += 1

        features = get_features_from_soup(soup)
        should_yield = True

        for k, v in features.items():
            if v is None:
                print('Unable to parse', k)
                should_yield = False
                if k not in missing_features:
                    missing_features[k] = 0
                missing_features[k] += 1

        if hasattr(features, 'price') and not (100000 < features['price'] < 2000000):
            print('Discarding property by outlier pricing', features['price'])
            should_yield = False

        if should_yield:
            print('Parsed ok')
            q_parsed += 1
            yield features

    print('miss count', missing_features)
    print('parsed {} of out {}, {}%'.format(q_parsed, q_parseable, int(q_parsed * 100 / q_parseable)))


def parse_energie_label(soup):
    label = _parse_tag_by_name_and_class(soup, "span", "energielabel")

    return fetch.quantify_energie_label(label)


def parse_address(soup):
    return _parse_tag_by_name_and_class(soup, "h1", "object-header-title")


def parse_postcode(soup):
    return normalise_postcode(_parse_tag_by_name_and_class(soup, "span", "object-header-subtitle"))


def _parse_tag_by_name_and_class(soup, tag_name, class_name):
    tag = soup.find(tag_name, class_=class_name)
    if not tag:
        return None

    if hasattr(tag, 'stripped_strings'):
        return next(tag.stripped_strings)

    if hasattr(tag, 'string'):
        return tag.string

    return None


def _parse_description_tag(soup):
    description_tag = soup.find("div", class_="object-description-body")
    if not description_tag:
        raise Exception('No object-description-body, wtf')

    return description_tag


def is_woonboot(soup):
    soort_woonhuis_tag = _parse_label(soup, "Soort woonhuis")
    if soort_woonhuis_tag and "woonboot" in soort_woonhuis_tag.lower():
        return True

    description_tag = _parse_description_tag(soup)
    for s in description_tag.stripped_strings:
        if 'woonboot' in s.lower():
            return True

    return False


def parse_lift(soup):
    description_tag = _parse_description_tag(soup)

    for s in description_tag.stripped_strings:
        if ' lift' in s.lower():
            return 1

    return 0


def parse_price(soup):
    return _parse_label(soup, "Vraagprijs", _get_first_dutch_number)


def parse_indoor_area(soup):
    return _parse_label(soup, "Woonoppervlakte", _get_first_dutch_number)


def parse_backgarden_area(soup):
    return _parse_label(soup, "Achtertuin", _get_first_dutch_number, 0)


def _parse_floor_from_description(soup):
    description_tag = _parse_description_tag(soup)

    for s in description_tag.stripped_strings:
        s = s.lower()
        if 'benedenwoning' in s:
            return 0
        if 'begane grond' in s:
            return 0
        if ' verdieping' in s:
            return _floor_to_number(_get_previous_word('verdieping', s))
        if ' etage' in s:
            return _floor_to_number(_get_previous_word('etage', s))

    return None


def _get_previous_word(needle, haystack):
    words = haystack.lower().split()
    needle = needle.lower()
    for i, w in enumerate(words[1:]):
        if w.find(needle) != -1:
            return words[i]

    return None


def _parse_floor_from_address(soup):
    address = parse_address(soup)
    if not address:
        return None

    floor = address.split(' ')[-1]

    return _floor_to_number(floor)


def _parse_floor_from_name(soup):
    if soup['property_name'].startswith('huis'):
        return 0

    if soup['property_name'].endswith('-huis'):
        return 0

    return None


def parse_floor_number(soup):
    ''' Above the 5th is all the same and hurts the predictor '''
    floor_number = _parse_floor_number(soup)
    if floor_number is not None and floor_number > 5:
        return 6

    return floor_number

def _parse_floor_number(soup):
    floor = _parse_floor_from_name(soup)
    if floor is not None:
        return floor

    floor = _parse_floor_from_address(soup)
    if floor is not None:
        return floor

    floor = _parse_floor_from_description(soup)
    if floor is not None:
        return floor

    floor = _parse_label(soup, 'Gelegen op', _floor_to_number, None)
    if floor is not None:
        return floor

    return None


def _parse_q_rooms_text(s):
    p = re.compile('(\d+) kamers \((\d+) slaapkamers\)', re.IGNORECASE)
    m = re.match(p, s)

    if m:
        return(tuple(map(int, m.group(1, 2))))
    else:
        return None, None


def parse_q_rooms(soup):
    return _parse_label(
        soup,
        "Aantal kamers",
        _parse_q_rooms_text,
        (None, None))


def parse_annual_lease(soup):
    ''' Return at tuple stating whether it pays erfpach and how much '''

    s = _parse_label(soup, "Eigendomssituatie", default=None)

    if s:
        s = s.lower()
        if s.startswith('volle eigendom'):
            # dueño de una pasión
            return False, 0

        lease_regexp = re.compile('erfpacht', re.IGNORECASE)
        if not lease_regexp.search(s):
            # no erfpacht, idem
            return False, 0

    # at this point we assume it does have an erfpacht
    return True, _parse_label(soup, 'Lasten', _get_first_dutch_number, 0)


def _first_element(seq):
    return seq[0]


def _parse_dutch_number(s):
    try:
        s = s.replace('.', '').replace(',', '.')
        return float(s) if '.' in s else int(s)
    except:
        return None


def _get_first_dutch_number(s):
    if not isinstance(s, str):
        return None

    for f in s.split(' '):
        p = _parse_dutch_number(f)
        if p is not None:
            return p

    return None


def _get_first_element(s):
    return (s.split(' '))[0] if isinstance(s, str) else None


def _floor_to_number(floor):
    if not isinstance(floor, str):
        return None

    floor = floor.replace('-', '')
    floor = floor.lower()
    if (
        floor.startswith('hs') or
        floor.startswith('huis') or
        floor.startswith('bg') or
        floor.find('begane') != -1
    ):
        return 0

    if (
        floor == 'i' or
        floor.startswith('i/') or
        floor.startswith('i-') or
        floor.startswith('i+') or
        floor == 'eerste' or
        floor == '1te' or
        floor == '1e' or
        floor == '1ste'
    ):
        return 1

    if (
        floor == 'ii' or
        floor.startswith('ii/') or
        floor.startswith('ii-') or
        floor.startswith('ii+') or
        floor == 'tweede' or
        floor == '2de' or
        floor == '2e'
    ):
        return 2

    if (
        floor == 'iii' or
        floor.startswith('iii/') or
        floor.startswith('iii-') or
        floor.startswith('iii+') or
        floor == 'derde' or
        floor == '3de' or
        floor == '3e' or
        floor == '3'
    ):
        return 3

    if (
        floor == 'iv' or
        floor.startswith('iv/') or
        floor.startswith('iv-') or
        floor.startswith('iv+') or
        floor == 'vierde' or
        floor == '4de' or
        floor == '4e'
    ):
        return 4

    if (
        floor == 'v' or
        floor.startswith('v/') or
        floor.startswith('v-') or
        floor.startswith('v+') or
        floor == 'vijfde' or
        floor == '5de' or
        floor == '5e'
    ):
        return 5

    p = re.compile('(\d+)(e|de)', re.IGNORECASE)
    m = re.match(p, floor)
    if m:
        return int(m.group(1))

    return None


def _parse_label(soup, name, func=lambda x: x, default=None):
    '''
    Parses soup for dt tag with name, assumes next element is the value,
    applies func to the string contained within the value. Defaults when
    no label tag is found.
    '''
    label_tag = soup.find("dt", string=name)
    if not label_tag:
        return default

    value_tag = label_tag.find_next()
    if not value_tag:
        raise Exception("Found {} tag but no value tag".format(name))

    # value could be the sole text or first element the sub-tags
    if hasattr(value_tag, "string") and value_tag.string:
        raw_value = value_tag.string
    elif hasattr(value_tag, "stripped_strings") and value_tag.stripped_strings:
        raw_value = next(value_tag.stripped_strings)
    else:
        raise Exception(
            "Found value tag but no relevant string for {}".format(value_tag))

    value = func(raw_value)

    return value if value is not None else default
