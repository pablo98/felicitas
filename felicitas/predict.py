import os
import pickle

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor

from . import fetch

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')
PREDICTOR_AND_SCALER_FILE = os.path.join(PROJECT_DIR, 'predictors/predictor.pickle')

# features to be collected from user input
input_features = [
    'postcode',
    'floor_number',
    'indoor_area',
    'garden_area',
    'energielabel',
    'lift',
    'annual_lease_amount']


# features that will actually feed the neural network
features_input_layer = [
    'energielabel',
    'area_price_per_mt2',
    'postcode_asking_price_per_mt2',
    'is_ground_floor',
    'is_binnen_de_ring',
    'is_not_binnen_de_ring',
    'has_erfpacht',
    'lat',
    'lng',
    'lift',
    'indoor_area',
    'garden_area',
    'annual_lease_amount',
    'floor_number']

target = 'price'


def save_predictor_and_scaler(predictor, scaler, FILE):
    with open(FILE, 'wb') as fh:
        pickle.dump((predictor, scaler), fh, pickle.HIGHEST_PROTOCOL)

def load_predictor_and_scaler(FILE):
    if not os.path.exists(FILE):
        return None

    with open(FILE, 'rb') as fh:
        pas = pickle.load(fh)

    return pas or None

def get_dataframe_from_dict(features):
    feature_dict = {k: [v] for k, v in features.items()}
    df = pd.DataFrame.from_dict(feature_dict)

    return df

def predict_from_raw_features(features):
    return predict_from_features(get_features_from_input(features))

def predict_from_features(features):
    if type(features) is dict:
        features = get_dataframe_from_dict(features)

    predictor, scaler = load_predictor_and_scaler(PREDICTOR_AND_SCALER_FILE)
    price = predict(features, predictor, scaler)

    return price

def get_training_data(train_and_test):
    # filter data
    X = train_and_test[features_input_layer]
    y = train_and_test[[target]]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)
    y_train = y_train.values.flatten()
    y_test = y_test.values.flatten()

    return X_train, y_train, X_test, y_test

def generate_predictor_and_scaler(X_train, y_train, X_test, y_test):
    # nomalize, standarise, scale, whatever
    # scaler = preprocessing.StandardScaler()
    scaler = preprocessing.MinMaxScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    predictor = get_neural_network(X_train, y_train, X_test, y_test)
    if predictor is None:
        print('Unable to get predictor')
        return None, None

    return predictor, scaler

def predict(feature_df, predictor, scaler):
    X = feature_df[features_input_layer]
    X = X.values.reshape(1, -1)
    X = scaler.transform(X)
    y = int(predictor.predict(X))

    return y

def get_features_from_input(input_fs):
    fs = {}
    postcode = input_fs['postcode']
    fs['floor_number'] = input_fs['floor_number']
    fs['indoor_area'] = input_fs['indoor_area']
    fs['garden_area'] = input_fs['garden_area']
    fs['annual_lease_amount'] = input_fs['annual_lease_amount']
    fs['area_price_per_mt2'] = next(fetch.get_price_per_mt2_from_postcode(postcode))
    fs['postcode_asking_price_per_mt2'] = next(fetch.get_asking_price_per_mt2_from_postcode(postcode))
    fs['areacode'] = postcode[:4]
    fs['energielabel'] = fetch.quantify_energie_label(input_fs['energielabel'])
    fs['lat'], fs['lng'] = next(fetch.get_coords_from_postcode(postcode))
    fs['is_ground_floor'] = 1 if fs['floor_number'] == 0 else 0
    fs['is_binnen_de_ring'] = 1 if fetch.is_binnen_de_ring(fs['areacode']) else 0
    fs['is_not_binnen_de_ring'] = 0 if fs['is_binnen_de_ring'] else 1
    fs['has_erfpacht'] = 1 if fs['annual_lease_amount'] else 0
    fs['lift'] = 1 if input_fs['lift'] else 0

    for k, v in fs.items():
        if v is None:
            raise Exception('Unable to parse {} feature'.format(k))

    return fs

def display_prediction_judgement(features):
    y_predicted = predict_from_features(features)
    y = features['price']
    error = int(y_predicted * 100 / y)
    print('{} is listed for {}, I had predicted {}, ({}%)'.format(
        features['name'], y, y_predicted, error))

def get_neural_network(X_train, y_train, X_test, y_test):
    alphas = [0.03, 0.01, 0.003, 0.001, 0.0003, 0.0001, 0.00003, 0.00001, 0.000003, 0.000001]
    best_score = 0
    best_predictor = None

    for alpha in alphas:
        predictor = MLPRegressor(
                solver='lbfgs',
                alpha=alpha,
                hidden_layer_sizes=(24, 24, 24, 24, 24),
                early_stopping=True,
                max_iter=700,
                # activation='relu',
                random_state=1)
        score = predictor.fit(X_train, y_train).score(X_test, y_test)
        if score > best_score:
            best_score = score
            best_predictor = predictor
            print(score, alpha)

    return best_predictor

