import os
import pandas as pd
import pickle

from felicitas.postcode_geo_mapper import PostcodeGeoMapper
from felicitas.postcode_price_mapper import PostcodePriceMapper
from felicitas.postcode_mapper import PostcodeMapper

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')

post2geo_pickle_file = os.path.join(PROJECT_DIR, 'data/postcodes_geo.pickle')
post2price_pickle_file = os.path.join(PROJECT_DIR, 'data/postcodes_price.pickle')
post2mean_asking_price_pickle_file = os.path.join(PROJECT_DIR, 'data/mean_asking_price_per_postcode.pickle')
area2mean_asking_price_pickle_file = os.path.join(PROJECT_DIR, 'data/mean_asking_price_per_area.pickle')

inner_areacodes = [
    1011, 1012, 1015, 1016, 1017, 1018, 1019,
    1051, 1052, 1053, 1054, 1055, 1055, 1056, 1057, 1058, 1059,
    1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079,
    1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098
]

def get_asking_price_per_mt2_from_postcode(postcode):
    asking_price_postcode_mapper = PostcodeMapper(post2mean_asking_price_pickle_file)
    asking_price_areacode_mapper = PostcodeMapper(area2mean_asking_price_pickle_file)

    while True:
        yield asking_price_postcode_mapper.get(postcode) or asking_price_areacode_mapper.get(postcode[:4])

def get_coords_from_postcode(postcode):
    geo_mapper = PostcodeGeoMapper(post2geo_pickle_file)

    while True:
        coords = geo_mapper.get(postcode)
        yield tuple(coords) if coords else (None, None)


def get_price_per_mt2_from_postcode(postcode):
    price_mapper = PostcodePriceMapper(post2price_pickle_file, get_coords_from_postcode)

    while True:
        yield price_mapper.get(postcode)

def quantify_energie_label(label):
    if not label:
        return 0

    label = label.upper()

    if label.startswith('A'):
        return 7
    if label.startswith('B'):
        return 6
    if label.startswith('C'):
        return 5
    if label.startswith('D'):
        return 4
    if label.startswith('E'):
        return 3
    if label.startswith('F'):
        return 2
    if label.startswith('G'):
        return 1

    return 0


def is_binnen_de_ring(areacode):
    return int(areacode) in inner_areacodes
