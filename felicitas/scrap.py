from time import sleep
import os
import re

import requests
from bs4 import BeautifulSoup


def scrap_searches(search_paths):
    for path in search_paths:
        walk_search(path, 0)


def get_page(path):
    base_url = 'http://www.funda.nl'
    print('connecting to', base_url + path)
    r = requests.get(base_url + path, headers=get_request_headers())
    sleep(2)
    return r.text


def get_request_headers():
    return {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'nl,en-US;q=0.7,en;q=0.3',
        'Cookie': 'SNLB2=12-002;Path=/;Domain=.funda.nl' \
        '; D_IID=A792BD32-E43D-35F4-823C-BF09C21F2CAC; D_UID=FDE8950D-3908-3E22-9BF3-CB5923FF6C20' \
        '; D_ZID=ADDA15ED-0B60-3F1D-924E-469DA07CFE0D; D_ZUID=4D2008B4-DC25-320F-A959-F0CFB3F1D1E1; D_HID=362CB664-AFF2-3970-95E2-E646F7DD6F1D' \
        '; D_SID=194.151.204.87:8LLnT7MNqRw1n5rgkF5yNj8c7XwpuPv1eaMYQHT9CZk; INLB=01-005; websitePreference=desktop' \
        '; .ASPXANONYMOUS=B1anR1r2mhs9JjxgNEPjjDhS0eyAhT3Vh3Xr1-K0DKmAu3ij0Hgiyj935AhqE00CENO0KZ1foi9T0mfi7BaietYvG-pX3FT9Ev5Al36SranFZEdaMvolDcpEck67iQHKbgU9NYa67hbJsjc2hqUHSpTmTF81'\
        '; lzo=koop=%2fkoop%2famsterdam%2f3-dagen%2f; optimizelyEndUserId=oeu1503475116424r0.5327337653062065' \
        '; html-classes=js supports-placeholder; fonts-loaded=true; cookiesAccepted_8=8',
        'DNT': '1',
        'Host': 'www.funda.nl',
        'Pragma': 'no-cache',
        'Referer': 'https://www.funda.nl/koop/amsterdam/3-dagen/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:54.0) Gecko/20100101 Firefox/54.0'}


def walk_search(path, depth):
    html = get_page(path)
    walk_page(html)

    next_page_path = get_next_page_path(html)
    if next_page_path and depth < 200:
        walk_search(next_page_path, depth + 1)


def walk_page(html):
    for path in get_properties_paths(html):
        scrap_property(path)


def get_properties_paths(html):
    soup = BeautifulSoup(html, "lxml")
    titles = soup.find_all("h3", class_="search-result-title")
    return [h.parent['href'] for h in titles]


def get_next_page_path(html):
    soup = BeautifulSoup(html, "lxml")
    url = soup.find("a", rel="next")
    if url:
        return url['href']
    else:
        return None


def scrap_property(path):
    ''' fetches property page, persists html and processes it '''
    filename = get_property_filename(path)

    # do not overwrite files
    try:
        if os.path.exists(filename):
            if os.path.getsize(filename) == 0:
                print(filename, "already present in filesystem, but zero bytes. Weird but will re download it.")
            else:
                print(path, "already present in filesystem, skipping")
                return

    except Exception as e:
        print("Got this error but will continue", str(e))

    # persist it
    html = get_page(path)
    with open(filename, 'w') as f:
        f.write(html)


def get_property_filename(url_path):
    base_path = os.path.dirname(os.path.realpath(__file__)) \
        + '/../data/properties_html/'
    _, _, _, name, _ = url_path.split('/')

    # make sure we hack funda and not the other way round
    name_regexp = re.compile('^[-a-zA-Z0-9]+$')
    if not name_regexp.match(name):
        raise Exception('invalid property name: ' + name)

    return base_path + name
