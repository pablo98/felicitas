#!/usr/bin/env python

import os
import sys

import felicitas.parse as parse
import felicitas.predict as predict

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')
PREDICTOR_AND_SCALER_FILE = os.path.join(PROJECT_DIR, 'predictors/predictor.pickle')
PROPERTIES_PATH = os.path.join(PROJECT_DIR, 'data/properties_html/')

# fetch file
features = parse.get_features_from_file(PROPERTIES_PATH + sys.argv[1])

# parse features
print(features, '\n###########################\n')

# predict asking price
predict.display_prediction_judgement(features)
