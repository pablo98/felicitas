import os
import unittest

import felicitas.parse as parse
import felicitas.fetch as fetch

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')

class TestPropertyParser(unittest.TestCase):
    ''' Slightly broader than unit tests '''

    samples = \
        'appartement-49167751-marco-polostraat-70-hs', \
        'appartement-49174953-geldersekade-11-iii', \
        'huis-49174071-ijburglaan-409', \
        'appartement-49188811-admiraal-de-ruijterweg-527-hs', \
        'huis-49181310-ijburglaan-291', \
        'appartement-49191863-maria-austriastraat-6', \
        'appartement-49004609-westerdok-362', \
        'appartement-49112259-maasstraat-41-iii-iv', \
        'appartement-49270663-diezestraat-32-ii-iii', \
        'appartement-49358042-vogelenzangstraat-43', \
        'huis-85692196-nieuwendammerkade-6', \
        'huis-85683077-prinsengracht-136-g', \
        'appartement-49296996-badhuiskade-bouwnr-039'

    def setUp(self):
        ''' read and parse properties html samples '''
        f = parse.get_property_soup
        base_path = os.path.join(PROJECT_DIR, 'tests/data/properties_html/')
        self.soups = [f(base_path + s) for s in self.samples]

    def test_is_binnen_de_ring(self):
        postcode = parse.parse_postcode(self.soups[0])
        self.assertTrue(fetch.is_binnen_de_ring(postcode[:4]), postcode[:4])

        postcode = parse.parse_postcode(self.soups[12])
        self.assertFalse(fetch.is_binnen_de_ring(postcode[:4]), postcode[:4])

    def test_address_parser(self):
        address = parse.parse_address(self.soups[0])
        self.assertEqual(address, 'Marco Polostraat 70 hs', self.samples[0])

        address = parse.parse_address(self.soups[1])
        self.assertEqual(address, 'Geldersekade 11 III', self.samples[1])

    def test_floor_number_parser(self):
        floor = parse.parse_floor_number(self.soups[0])
        self.assertEqual(floor, 0, self.samples[0])

        floor = parse.parse_floor_number(self.soups[1])
        self.assertEqual(floor, 3, self.samples[1])

        floor = parse.parse_floor_number(self.soups[6])
        self.assertEqual(floor, 0, self.samples[6])

        floor = parse.parse_floor_number(self.soups[7])
        self.assertEqual(floor, 3, self.samples[7])

        floor = parse.parse_floor_number(self.soups[8])
        self.assertEqual(floor, 2, self.samples[8])

        floor = parse.parse_floor_number(self.soups[9])
        self.assertEqual(floor, 0, self.samples[9])

    def test_get_first_dutch_number(self):
        self.assertEqual(699000, parse._get_first_dutch_number('€ 699.000 k.k.'))
        self.assertEqual(699, parse._get_first_dutch_number(' 699 k.k.'))
        self.assertEqual(1.5, parse._get_first_dutch_number('€ 1,5 k.k.'))
        self.assertEqual(343123, parse._get_first_dutch_number('First number is not gonna be 34,.r but 343.123'))

    def test_label_parser(self):
        price_string = parse._parse_label(self.soups[2], 'Vraagprijs')
        self.assertEqual('€ 699.000 k.k.', price_string)

    def test_get_previous_word(self):
        self.assertEqual(
            'tweede',
            parse._get_previous_word('Verdieping', 'gelegen op tweede verdieping'))

        self.assertEqual(
            '3de',
            parse._get_previous_word('etage', 'gelegen op 3de Etage'))

        self.assertEqual(
            None,
            parse._get_previous_word('gelegen', 'gelegen op tweede verdieping'))

        self.assertEqual(
            None,
            parse._get_previous_word('will not find', 'gelegen op tweede verdieping'))

        self.assertEqual(
            'tweede',
            parse._get_previous_word('verdieping', 'mooie apartment, gelegen op de tweede verdieping'))

    def test_dutch_number_tester(self):
        self.assertIsNotNone(False, parse._parse_dutch_number('€'))
        self.assertIsNotNone(False, parse._parse_dutch_number('34.5'))
        self.assertIsNotNone(False, parse._parse_dutch_number('370 000'))
        self.assertIsNotNone(False, parse._parse_dutch_number('370r000'))
        self.assertIsNotNone(False, parse._parse_dutch_number('370s'))
        self.assertIsNotNone(False, parse._parse_dutch_number('4no a flot'))
        self.assertIsNotNone(False, parse._parse_dutch_number('sarasa3'))
        self.assertIsNotNone(False, parse._parse_dutch_number('sarasa3'))
        self.assertIsNotNone(True, parse._parse_dutch_number('0'))
        self.assertIsNotNone(True, parse._parse_dutch_number('0,000'))
        self.assertIsNotNone(True, parse._parse_dutch_number('12340'))
        self.assertIsNotNone(True, parse._parse_dutch_number('370'))
        self.assertIsNotNone(True, parse._parse_dutch_number('3,700'))
        self.assertIsNotNone(True, parse._parse_dutch_number('370.000'))
        self.assertIsNotNone(True, parse._parse_dutch_number('3.700.000'))
        self.assertIsNotNone(True, parse._parse_dutch_number('7.127,54'))

    def test_dutch_number_parser(self):
        self.assertEqual(0.333, parse._parse_dutch_number('0,333'))
        self.assertEqual(35.54, parse._parse_dutch_number('35,54'))
        self.assertEqual(3700000, parse._parse_dutch_number('3.700.000'))
        self.assertEqual(370000, parse._parse_dutch_number('370.000'))
        self.assertEqual(3700, parse._parse_dutch_number('3.700'))
        self.assertEqual(370, parse._parse_dutch_number('370'))
        self.assertEqual(7127.54, parse._parse_dutch_number('7.127,54'))

    def test_price_parser(self):
        price = parse.parse_price(self.soups[0])
        self.assertEqual(price, 370000, self.samples[0])

        price = parse.parse_price(self.soups[1])
        self.assertEqual(price, 325000, self.samples[1])

    def test_postcode_parser(self):
        postcode = parse.parse_postcode(self.soups[0])
        self.assertEqual(postcode, '1057WS', self.samples[0])

        postcode = parse.parse_postcode(self.soups[1])
        self.assertEqual(postcode, '1011EH', self.samples[1])

    def test_lift_parser(self):
        lift = parse.parse_lift(self.soups[0])
        self.assertEqual(lift, 0, self.samples[0])

        lift = parse.parse_lift(self.soups[5])
        self.assertEqual(lift, 1, self.samples[5])

    def test_indoor_area_parser(self):
        area = parse.parse_indoor_area(self.soups[0])
        self.assertEqual(area, 72, self.samples[0])

        area = parse.parse_indoor_area(self.soups[1])
        self.assertEqual(area, 64, self.samples[1])

    def test_annual_lease_parser(self):
        does_pay, amount = parse.parse_annual_lease(self.soups[0])
        self.assertEqual(does_pay, True, self.samples[0])
        self.assertEqual(amount, 307.84, self.samples[0])

        does_pay, amount = parse.parse_annual_lease(self.soups[1])
        self.assertEqual(does_pay, False, self.samples[1])
        self.assertEqual(amount, 0, self.samples[1])

        does_pay, amount = parse.parse_annual_lease(self.soups[2])
        self.assertEqual(does_pay, True, self.samples[2])
        self.assertEqual(amount, 0, self.samples[2])

        does_pay, amount = parse.parse_annual_lease(self.soups[3])
        self.assertEqual(does_pay, True, self.samples[3])
        self.assertEqual(amount, 636.72, self.samples[3])

        does_pay, amount = parse.parse_annual_lease(self.soups[4])
        self.assertEqual(does_pay, True, self.samples[4])
        self.assertEqual(amount, 7127.62, self.samples[4])

    def test_backgarden_area_parser(self):
        backgarden_area = parse.parse_backgarden_area(self.soups[0])
        self.assertEqual(backgarden_area, 52, self.samples[0])

        backgarden_area = parse.parse_backgarden_area(self.soups[1])
        self.assertEqual(backgarden_area, 0, self.samples[1])

        backgarden_area = parse.parse_backgarden_area(self.soups[2])
        self.assertEqual(backgarden_area, 0, self.samples[2])

        backgarden_area = parse.parse_backgarden_area(self.soups[3])
        self.assertEqual(backgarden_area, 60, self.samples[3])

    def test_q_rooms_parser(self):
        rooms, bedrooms = parse.parse_q_rooms(self.soups[0])
        self.assertEqual((rooms, bedrooms), (3, 2), self.samples[0])

        rooms, bedrooms = parse.parse_q_rooms(self.soups[2])
        self.assertEqual((rooms, bedrooms), (5, 2), self.samples[2])

        rooms, bedrooms = parse.parse_q_rooms(self.soups[3])
        self.assertEqual((rooms, bedrooms), (4, 3), self.samples[3])

    def test_is_woonboot(self):
        self.assertTrue(parse.is_woonboot(self.soups[10]), self.samples[10])
        self.assertTrue(parse.is_woonboot(self.soups[10]), self.samples[11])


if __name__ == '__main__':
    unittest.main()
