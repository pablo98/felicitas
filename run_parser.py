#!/usr/bin/env python

import csv
import os

import felicitas.parse as parse

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')


def main():
    # @todo put conf aside
    source_path = os.path.join(PROJECT_DIR, 'data/properties_html/')
    target_file = os.path.join(PROJECT_DIR, 'data/properties3.csv')

    files = (source_path + f for f in os.listdir(source_path))
    properties = parse.parse_properties_files(files)

    with open(target_file, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, parse.feature_names)
        writer.writeheader()
        writer.writerows(properties)


if __name__ == "__main__":
    main()
