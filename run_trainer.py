#!/usr/bin/env python

import os
import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import felicitas.predict as predict
import felicitas.parse as parse

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')
PROPERTIES_CSV_FILE = os.path.join(PROJECT_DIR, 'data/properties3.csv')
PREDICTOR_AND_SCALER_FILE = os.path.join(PROJECT_DIR, 'predictors/predictor.pickle')

def main():
    # get data
    data = pd.read_csv(PROPERTIES_CSV_FILE)

    # shuffle & split
    cross_validation = data[-30:]
    data = data.reindex(np.random.permutation(data.index))
    train_and_test = data[:-20]
    X_train, y_train, X_test, y_test = predict.get_training_data(train_and_test)
    print('training with', len(X_train), 'samples, testing with', len(X_test))

    # train and save a new neural network
    predictor, scaler = predict.generate_predictor_and_scaler(X_train, y_train, X_test, y_test)
    if not predictor or not scaler:
        print('Unable to get a predictor')
        sys.exit(1)

    predict.save_predictor_and_scaler(predictor, scaler, PREDICTOR_AND_SCALER_FILE)

    for i, features in cross_validation.iterrows():
        predict.display_prediction_judgement(features)

    display_predictor_stats(predictor, scaler, X_train, y_train, X_test, y_test)


def display_predictor_stats(predictor, scaler, X_train, y_train, X_test, y_test):
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    y_predicted = predictor.predict(X_test)
    y_train_predicted = predictor.predict(X_train)
    y_predicted = list(map(lambda x: int(x), y_predicted))

    # printout some cold numbers
    diff = y_train_predicted / y_train
    ratio = abs(1 - diff)
    print('mean ', np.mean(ratio))
    print('stddev ', np.std(ratio))
    print('min ', np.min(diff))
    print('max ', np.max(diff))

    # plot train vs test accuracy
    plt.scatter(y_predicted, y_test, s=10, alpha=0.8)
    plt.scatter(y_train_predicted, y_train, s=10, alpha=0.1)
    plt.show()


if __name__ == "__main__":
    main()
