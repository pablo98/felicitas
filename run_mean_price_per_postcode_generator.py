#!/usr/bin/env python

import csv
import os
import pickle

from felicitas import parse, fetch

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')

def main():
    # @todo put conf aside
    source_path = os.path.join(PROJECT_DIR, 'data/properties_html/')
    target_path = os.path.join(PROJECT_DIR, 'data/')

    files = (source_path + f for f in os.listdir(source_path))
    properties = list(parse.parse_properties_meter_price_per_postcode(files))
    df = parse.get_dataframe_from_html_features(properties)

    d = df.groupby('areacode')['price_per_mt2'].mean().to_dict()
    save_pickle(fetch.area2mean_asking_price_pickle_file, d)
    print(d)

    d = df.groupby('postcode')['price_per_mt2'].mean().to_dict()
    save_pickle(fetch.post2mean_asking_price_pickle_file, d)
    print(d)

def save_pickle(f, p):
    with open(f, 'wb') as fh:
        pickle.dump(p, fh, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()
