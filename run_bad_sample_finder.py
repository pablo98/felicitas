#!/usr/bin/env python

import os
import pickle
import sys

import numpy as np
import pandas as pd

from felicitas.predict import predict_from_features

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')
PROPERTIES_CSV_FILE = os.path.join(PROJECT_DIR, 'data/properties3.csv')

def main():
    # get data
    samples = pd.read_csv(PROPERTIES_CSV_FILE)

    for i, features in samples.iterrows():
        y_predicted = predict_from_features(features)
        ratio = y_predicted / features['price']
        if ratio > 1.8 or ratio < 0.5:
            print('Failed badly; it was {} but I said {}'.format(features['price'], y_predicted), features)

if __name__ == "__main__":
    main()
