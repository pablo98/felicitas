#!/usr/bin/env python

import os
import pickle
import sys

import numpy as np
import pandas as pd

import felicitas.predict as predict

PROJECT_DIR = os.environ.get('FELICITAS_PROJECT_DIR')
PROPERTIES_CSV_FILE = os.path.join(PROJECT_DIR, 'data/properties3.csv')

def main():
    # get data
    data = pd.read_csv(PROPERTIES_CSV_FILE)

    # shuffle & split
    data = data.reindex(np.random.permutation(data.index))
    cross_validation = data[:20]

    for i, features in cross_validation.iterrows():
        predict.display_prediction_judgement(features)

if __name__ == "__main__":
    main()
